﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorphemeSplitTests
{
    public class Morpheme
    {
        public enum AffixType
        {
            Prefix,
            Suffix,
            Infix
        }

        public static Morpheme FromText(string data)
        {
            if (data.EndsWith("-"))
            {
                return new Morpheme(AffixType.Prefix, data.Substring(0, data.Length - 1), true);
            }
            else if (data.StartsWith("-"))
            {
                return new Morpheme(AffixType.Suffix, data.Substring(1), true);
            }
            else
            {
                return null;
            }
        }

        private AffixType _type;
        private string _text;
        private bool _is_bound;

        public AffixType Type { get => _type;}
        public string Text { get => _text;}
        public bool IsBound { get => _is_bound;}

        public Morpheme(AffixType type, string text, bool isBound)
        {
            _type = type;
            _text = text;
            _is_bound = isBound;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            //if (_is_bound)
            //{
            //    sb.Append("[B]");
            //}
            //else
            //{
            //    sb.Append("[F]");
            //}
            switch (_type)
            {
                case AffixType.Prefix:
                    sb.Append(_text).Append("-");
                    break;  
                case AffixType.Suffix:
                    sb.Append("-").Append(_text);
                    break;
                case AffixType.Infix:
                    sb.Append("-").Append(_text).Append("-");
                    break;
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Morpheme))
            {
                return false;
            }
            else
            {
                Morpheme toCompare = obj as Morpheme;
                return _type == toCompare._type && _text == toCompare._text && _is_bound == toCompare._is_bound;
            }
        }

        public override int GetHashCode()
        {
            return _type.GetHashCode() + _type.GetHashCode() + _is_bound.GetHashCode();
        }

    }
}
