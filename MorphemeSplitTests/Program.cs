﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorphemeSplitTests
{
    class Program
    {

        static void Main(string[] args)
        {
            HashSet<Morpheme> affixes = new HashSet<Morpheme>();
            HashSet<string> dictionary = new HashSet<string>();

            using (var fs = new FileStream("Morphemes.txt", FileMode.Open))
            {
                using (var sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        Morpheme temp = Morpheme.FromText(sr.ReadLine().Trim());
                        affixes.Add(temp);
                    }
                }
            }
            using (var fs = new FileStream("Dictionary.txt", FileMode.Open))
            {
                using (var sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        dictionary.Add(sr.ReadLine().Trim().ToLower());
                    }
                }
            }

            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("joyfulness", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("antiseptic", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("infuriate", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("cooperate", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("misspell", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("solitude", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("payment", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("canadian", affixes), dictionary).Select((m) => m.ToString()).ToArray()));
            Console.WriteLine(String.Join(" + ", AdjustCroppedStems(ExtractMorphemes("italian", affixes), dictionary).Select((m) => m.ToString()).ToArray()));



            Console.ReadKey();
        }

        static List<Morpheme> ExtractMorphemes(string input, HashSet<Morpheme> affixes)
        {
            List<Morpheme> extractedPrefix = new List<Morpheme>();
            Stack<Morpheme> extractedSuffix = new Stack<Morpheme>();
            string remainder = input.ToLower();

            //Check for Suffixes
            for (int i = 0; i < remainder.Length; ++i)//Pour favoriser les suffix plus long
            {
                Morpheme temp = new Morpheme(Morpheme.AffixType.Suffix, remainder.Substring(i, remainder.Length - i), true);
                if (affixes.Contains(temp))
                {
                    extractedSuffix.Push(temp);
                    remainder = remainder.Substring(0, i);
                    i = 0;
                }
            }

            //Check for Prefixes
            for (int i = remainder.Length - 1; i > 0; --i)//Pour favoriser les prefix plus long
            {
                Morpheme temp = new Morpheme(Morpheme.AffixType.Prefix, remainder.Substring(0, i), true);
                if (affixes.Contains(temp))
                {
                    extractedPrefix.Add(temp);
                    remainder = remainder.Substring(i, remainder.Length - i);
                    i = remainder.Length - 1;
                }
            }

            List<Morpheme> res = new List<Morpheme>();

            foreach (var m in extractedPrefix)
            {
                res.Add(m);
            }

            if (remainder.Length != 0)
            {
                Morpheme.AffixType typeTemp = Morpheme.AffixType.Infix;
                if (res.Count > 0 && extractedSuffix.Count > 0)
                {
                    typeTemp = Morpheme.AffixType.Infix;
                }
                else if (res.Count > 0)
                {
                    typeTemp = Morpheme.AffixType.Suffix;
                }
                else
                {
                    typeTemp = Morpheme.AffixType.Prefix;
                }
                res.Add(new Morpheme(typeTemp, remainder, false));
            }

            while (extractedSuffix.Count > 0)
            {
                res.Add(extractedSuffix.Pop());
            }

            return res;
        }

        static List<Morpheme> AdjustCroppedStems(List<Morpheme> input, HashSet<string> dic)
        {
            Morpheme toAdjust = input.FirstOrDefault((m) => !m.IsBound);
            if (toAdjust != null)
            {
                if (!dic.Contains(toAdjust.Text))
                {
                    foreach (var v in new List<String> { "a", "e", "i", "o", "u", "y" })
                    {
                        if (dic.Contains(toAdjust.Text + v))
                        {
                            List<Morpheme> res = new List<Morpheme>();
                            foreach (var m in input)
                            {
                                if (m.IsBound)
                                {
                                    res.Add(m);
                                }
                                else
                                {
                                    res.Add(new Morpheme(toAdjust.Type, toAdjust.Text + v, false));
                                }
                            }
                            return res;
                        }
                    }
                }
            }
            return input;
        }
    }
}
